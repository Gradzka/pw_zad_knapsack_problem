// Wykonano na podstawie: 
// https://rosettacode.org/wiki/Knapsack_problem/0-1#C
// http://www-users.mat.uni.torun.pl/~henkej/knapsack.pdf

#include <string>
#include <vector>
#include "fstream"
#include <iostream>
#include <omp.h>
using namespace std;

struct Knapsack_Item
{
	string name;
	int weight;
	int value;
};

bool *knapsack_function(vector <Knapsack_Item> knapsack_vector, int num_of_items_to_knap, int knap_capacity, double &start, double &stop) {
	int i, j, a, b, *mm, **m;
	bool *packed_in_knap;
	mm = (int*)calloc((num_of_items_to_knap + 1) * (knap_capacity + 1), sizeof(int));
	m = (int **)malloc((num_of_items_to_knap + 1) * sizeof(int *));
	m[0] = mm;

	start = omp_get_wtime();
	for (i = 1; i <= num_of_items_to_knap; i++) {
		m[i] = &mm[i * (knap_capacity + 1)];

		#pragma omp parallel for shared(knap_capacity,knapsack_vector,m), private(j,a,b)
		for (j = 0; j <= knap_capacity; j++) {
			if (knapsack_vector[i - 1].weight > j) {
				m[i][j] = m[i - 1][j];
			}
			else {
				a = m[i - 1][j];
				b = m[i - 1][j - knapsack_vector[i - 1].weight] + knapsack_vector[i - 1].value;
				m[i][j] = a > b ? a : b;
			}
		}
	}

	packed_in_knap = (bool*)calloc(num_of_items_to_knap, sizeof(bool));
	for (i = num_of_items_to_knap, j = knap_capacity; i > 0; i--) {
		if (m[i][j] > m[i - 1][j]) {
			packed_in_knap[i - 1] = 1;
			j -= knapsack_vector[i - 1].weight;
		}
	}
	stop = omp_get_wtime();
	free(mm);
	free(m);
	return packed_in_knap;
}
int main()
{
	vector <Knapsack_Item> knapsack_vector;
	Knapsack_Item item;

	fstream file;
	file.open("knapsack.txt", ios::in);

	int knap_capacity;
	cout << "Podaj pojemnosc plecaka: ";
	cin >> knap_capacity;
	while (knap_capacity<=0)
	{
		cout << "Podano nieprawidlowa pojemnosc plecaka! Podaj pojemnosc plecaka: ";
		cin >> knap_capacity;
	}
	cout << "\nSposrod podanych przedmiotow do plecaka o pojemnosci " << knap_capacity << " dag mozna zapakowac:" << endl;
	cout <<"Waga\tWartosc\tNazwa" << endl;

	if (file.is_open())
	{
		while (!file.eof()){
			file >> item.name >> item.weight >> item.value;
			//cout << item.name << " " << item.weight << " " << item.value;
			knapsack_vector.push_back(item);
			//cout << knapsack_vector[0].name;
			cout << knapsack_vector[knapsack_vector.size() - 1].weight << "\t" << knapsack_vector[knapsack_vector.size() - 1].value <<"\t"<< knapsack_vector[knapsack_vector.size() - 1].name << "\n";
		}
	}
	file.close();

	if (knapsack_vector.size() == 0) //w przypadku pustego pliku!
	{
		cout << "Brak informacji o przedmiotach do zapakowania do plecaka!\n";
		return 0;
	}

	int i, num_of_items_to_knap, total_weight_of_items = 0, total_value_of_items = 0;
	bool *packed_in_knap;
	num_of_items_to_knap = knapsack_vector.size();

	double start = 0;
	double stop = 0;
	double start_stop_sub = 0;
	packed_in_knap = knapsack_function(knapsack_vector, num_of_items_to_knap, knap_capacity,start,stop);
	start_stop_sub = stop - start;
	cout << endl << "Rozwiazanie problemu plecakowego:" << endl;
	cout <<endl<< "Waga\tWartosc\tNazwa" << endl;

	for (i = 0; i < num_of_items_to_knap; i++) 
	{
		if (packed_in_knap[i]) //packed_in_knap zawiera info, czy dany produkt ma byc spakowany (1), czy nie (0)
		{
			cout << knapsack_vector[i].weight << "\t" << knapsack_vector[i].value << "\t" << knapsack_vector[i].name << "\n";
			total_weight_of_items += knapsack_vector[i].weight;
			total_value_of_items += knapsack_vector[i].value;
		}
	}
	
	cout << "-------------------------" << endl;
	cout << total_weight_of_items << "\t"<< total_value_of_items <<"\tSUMA"<< endl;

	cout << "Czas wyznaczenia optymalnej warto�ci dla problemu plecakowego: " << start_stop_sub << endl;;

	/*for (int i = 0; i < num_of_items_to_knap; i++)
		std::cout<<knapsack_vector[i].name<<" ";*/

	return 0;
}